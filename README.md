Il progetto consiste nello sviluppo di un'applicazione e-commerce basata su wordpress e alcuni social plugins. Il tema "storefront" è stato modificato per introdurre funzionalità avanzate e tradotto in italiano.

Si può testare un esempio funzionante su http://prsi.mefiaflip.org

### Deployment

Entrare nella cartella pubblica del webserver o in locale. Generalmente i file sono visibili su var/www/public_html

* copiare il file .zip senza scompattarlo ne rinominarlo
* copiare il file installer.php nella stessa directory
* verificare che sia possibile scrivere file
* eseguire /installer.php da browser

###  Installer 
* Inserire i dati del database sql
* eseguire l'installazione


### Problemi noti
Quando si effettua il deployment è necessario verificare la struttura dei permalink.
Può capitare che venga mostrato un errore 404 se i permalink sono costruiti come ad esempio |%postname%|

In tal caso bisogna ripristinare le impostazioni di default e verificare che il file .htaccess

 sia configurato in questo modo: https://codex.wordpress.org/htaccess

### Team
Niccolò mascaro | mailto:niccolo.mas@gmail.com